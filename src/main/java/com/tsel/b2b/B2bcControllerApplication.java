package com.tsel.b2b;

import com.tsel.b2b.controller.CallbackController;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;
import java.sql.SQLException;

@SpringBootApplication
public class B2bcControllerApplication {

	public static void main(String[] args) throws IOException, SQLException {
		SpringApplication.run(B2bcControllerApplication.class, args);


		var output = new CallbackController();

//		output.charge(5555);
		output.actionCdrCreator();


	}

}
