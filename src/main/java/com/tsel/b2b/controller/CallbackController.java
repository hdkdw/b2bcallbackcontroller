package com.tsel.b2b.controller;


import com.tsel.b2b.utility.DB_Connection;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.dbutils.DbUtils;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Slf4j
public class CallbackController {


    /*
    * need to be clarified if a companyId can have more than 1 subscription package
    *
    * */
    public void charge(Integer companyId) throws SQLException {

        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        PreparedStatement updateCompSubs = null;
        PreparedStatement psSelectBulk = null;
        ResultSet rsSelectBulk =null;
        PreparedStatement psUpdateBulk = null;

        try{
            DB_Connection obj_DB_Connection = new DB_Connection();
            conn = obj_DB_Connection.getConnection();

            String query = "select companyId, a.usage, balance from companysubscription a join servicepackage b on a.servicePackageId = b.id where companyId=? and (b.serviceId=5 || b.serviceId =6) GROUP BY companyId;";
            ps = conn.prepareStatement(query, ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_READ_ONLY);
            ps.setInt(1, companyId);
            rs = ps.executeQuery();

            if (!rs.next()) {
                log.info("data companysubscription for companyId {} not found", companyId);
//            throw new SQLException("data companysubscription not found");
            }
            rs.beforeFirst(); //put cursor back to before first for calculation below
            while (rs.next()) {
                log.info(String.valueOf(rs.getInt("companyId") + "; " + rs.getInt("a.usage")+"; "+rs.getInt("balance")));

                int newUsage = rs.getInt("a.usage")+1;
                int newBalance = rs.getInt("balance")-1;

                if(rs.getInt("balance") !=0){
                    updateCompSubs = conn.prepareStatement("UPDATE companysubscription SET companysubscription.usage = ?, balance=? WHERE companyId = ? ");
                    updateCompSubs.setInt(1,newUsage);
                    updateCompSubs.setInt(2,newBalance);
                    updateCompSubs.setInt(3,companyId);
                    updateCompSubs.executeUpdate();
                }

                psSelectBulk = conn.prepareStatement("SELECT a.usage, balance FROM bulk a WHERE subscriptionId=?;");
                psSelectBulk.setInt(1,companyId);
                rsSelectBulk = psSelectBulk.executeQuery();

                while (rsSelectBulk.next()) {
                    if(rsSelectBulk.getInt("usage") !=0){

                        psUpdateBulk = conn.prepareStatement("UPDATE bulk SET bulk.usage =?, balance = ? WHERE subscriptionId = ?");
                        psUpdateBulk.setInt(1,rsSelectBulk.getInt("a.usage")+1);
                        psUpdateBulk.setInt(2,rsSelectBulk.getInt("balance")-1);
                        psUpdateBulk.setInt(3,companyId);
                        psUpdateBulk.executeUpdate();

                    }
                }
            }
        } finally {
            DbUtils.closeQuietly(rs);
            DbUtils.closeQuietly(ps);
            DbUtils.closeQuietly(updateCompSubs);
            DbUtils.closeQuietly(psSelectBulk);
            DbUtils.closeQuietly(rsSelectBulk);
            DbUtils.closeQuietly(psUpdateBulk);
            DbUtils.closeQuietly(conn);
        }

    }

    public void actionCdrCreator() throws SQLException, IOException {

        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            LocalDateTime dateTime = LocalDateTime.now();
            DateTimeFormatter timeStamp = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");

            String filePath = "./src/main/resources/static/" + "cdr"+dateTime.format(timeStamp) + ".csv";
            PrintWriter pw = new PrintWriter(new File(filePath));
            StringBuilder sb = new StringBuilder();

            DB_Connection obj_DB_Connection = new DB_Connection();
            conn = obj_DB_Connection.getConnection();

            String query = "SELECT b.executionTime, pilotNumber, transactionId FROM mepro_preprod.smsrequest a INNER JOIN mepro_preprod.smstransaction b ON a.id = b.requestId WHERE a.status = 'success' AND b.executionTime >= (CURDATE() - INTERVAL 1 DAY) ORDER BY a.id;";
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();

            while (rs.next()) {
                sb.append(rs.getString("executionTime"));
                sb.append(";");
                sb.append(rs.getString("pilotNumber"));
                sb.append(";");
                sb.append(rs.getString("transactionId"));
                sb.append(";");
                sb.append("4008");
                sb.append("\r\n");

            }

            pw.write("execution_time;pilot_number;transaction_id;nums\r\n");
            pw.write(sb.toString());
            pw.close();

            log.info("files cdr created");
        } catch (Exception ex) {
            log.info(String.valueOf(ex));
        } finally {
            DbUtils.closeQuietly(rs);
            DbUtils.closeQuietly(ps);
            DbUtils.closeQuietly(conn);
        }

    }
}






